# SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


"""
This package implements the concrete input methods.
"""


# Common constants
SPLIT_CHARACTER = ";"
